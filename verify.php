<?php 
    if( !isset($_POST['password']) )
        header("location:login.php") || die(); //user not logged in

    $pw = $_POST['password'];
    $host = $_SERVER['HTTP_HOST'];
    $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');

    $db = new mysqli("localhost", "root", "", "outlook");
    if($db->errno)
        die("Errore connessione al database!");

    $query = "SELECT * FROM users
              WHERE id_user = $pw";
    $result = $db->query($query);

    $newlocation = "login.php?error";

    if($result)
    {
        if($result->num_rows != 0)
        {
            $newlocation = "questions.php";
            //Rimuovo eventuali sessioni precedenti
            session_start();
            session_unset();
            session_destroy();
            //Inizializzo nuova sessione
            session_start();
            
            $_SESSION['user'] = $pw;
            $_SESSION['start_time'] = time();
        }
        $result->free();
    }
    
    $db->close();
    header("Location: http://$host$uri/$newlocation");
?>